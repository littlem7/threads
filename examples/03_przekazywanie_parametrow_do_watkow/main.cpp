#include <iostream>
#include <thread>
#include <bits/stl_vector.h>

// Do pierwszego sposobu
class BackgroundTask {
    private:
        int _x;
        double _y;

    public:
        BackgroundTask(int x, double y) : _x(x), _y(y) {}

        void operator()(){
            std::cout << _x << ", " << _y << std::endl;
        }
};


// Do drugiego sposobu
void f1(int n)
{
    for (int i = 0; i < 3; ++i)
    {
        std::cout << "Thread 1 executing\n";
        ++n; // increment of local copy
    }
}

void f2(int& n)
{
    for (int i = 0; i < 5; ++i)
    {
        std::cout << "Thread 2 executing\n";
        ++n;
    }
}


// Do trzeciego sposobu
void set_value(int index, std::vector<int> &data)
{
    data[index] = 7;
}

// Do czwartego sposobu
struct A{
    void non_static(){
        std::cout << "Metoda niestatyczna" << std::endl;
    }
};


int main() {
    // Pierwszy sposób
    BackgroundTask bt(1, 3.14);
    std::thread thd0(bt);
    thd0.join();

    // Drugi sposób
    int n = 0;
    std::thread thd1(f1, n + 1);            // przekazane przez wartosc
    std::thread thd2(f2, std::ref(n));      // przekazane przez referencje
    thd1.join();
    thd2.join();
    std::cout << "Final value of n is " << n << '\n';

    // Trzeci sposób
    std::vector<int> vec(2);
    std::cout << vec[0] << ", " << vec[1] << std::endl;

    std::thread thd3(set_value, 0, std::ref(vec));
    std::thread thd4([&vec]{ set_value(1, vec); });     // wykorzystanie obiektu domkniecia

    thd3.join();
    thd4.join();
    std::cout << vec[0] << ", " << vec[1] << std::endl;

    // Czwarty sposób
    std::thread thd5(&A::non_static, A());
    thd5.join();
}

// mozliwy output:
// 1, 3.14
// Thread 2 executing
// Thread 2 executing
// Thread 2 executing
// Thread 2 executing
// Thread 2 executing
// Thread 1 executing
// Thread 1 executing
// Thread 1 executing
// Final value of n is 5
// 0, 0
// 7, 7
// Metoda niestatyczna