/*
* Plik naglowkowy klasy Counter
* autor: Anna Bogusz
*/

#ifndef COUNTER_H
#define COUNTER_H

#include <mutex>

class Counter {
    public:
        // statyczne pole przechowujace wskaznik do utworzonej instancji 
        // singletonu; inicjalizowane za pomoca nullptr w Counter.cpp
        static Counter * generatorInstance;

        // pole inicjalizowane w Counter.cpp; mutex dba, by metody klasy byly
        // thread safe za pomoca std::lock_guard()
        static std::mutex mtx;

        // pole typu unsigned reprezentuje biezaca wartosc licznika; domyslnie 
        // inicjalizowana przez 7
        unsigned counterNum = 7;

        // metoda statyczna ktora odpowiada za stworzenie singletonu raz
        static Counter * getInstance();

        // destruktor dba o zarzadzanie pamiecia zaalokowana w metodzie getInstance
        ~Counter();

        // statyczna metoda typu void ktora bedzie wstrzymywala biezacy watek na sekunde, 
        // zwiekszala wartosc pola przekazanego jej w postaci referencji oraz wypisywala 
        // na ekran komunikat w formacie: [zmieniona wartosc] -> modified by thread: [id watku]
        static void increment(unsigned& value);

    private:
        // prywatny kontruktor domyslny do konstrukcji singletonu jedynie przez
        // metode getInstance()
        Counter(){}
};

#endif // COUNTER_H