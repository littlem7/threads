/*
* Plik źródłowy zadania THD
* autor: Anna Bogusz
*/

#include "THD.h"
#include <thread>

void incrementCounter(){
    Counter* instance = Counter::getInstance();
    std::thread(Counter::increment, std::ref(instance->counterNum)).detach();
}
