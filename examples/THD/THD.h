/*
* Plik naglowkowy zadania THD
* autor: Anna Bogusz
*/

#ifndef THD_H
#define THD_H

#include "Counter.h"

// funkcja typu void ktora tworzy nowy watek w tle z parametrem statycznej 
// metody inkrementujacej pole typu unsigned utworzonej (w funkcji) 
// instancji klasy Counter.
void incrementCounter();

#endif // THD_H