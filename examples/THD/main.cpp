#include <thread>
#include <vector>
#include "THD.h"

/*
* Prosze zaimplementowac klase Counter, ktora bedzie singletonem thread-safe;
* klasa zawiera pole typu unsigned o domyslnej wartosci 7 reprezentujaca wartosc
* poczatkowa licznika. Dodatkowo klasa ma implementowac statyczna metode typu void
* ktora bedzie wstrzymywala biezacy watek na sekunde, zwiekszala wartosc pola 
* przekazanego jej w postaci referencji oraz wypisywala na ekran komunikat w formacie:
* [zmieniona wartosc] -> modified by thread: [id watku]
*
* Nastepnie prosze zaimplementowac funkcje typu void incrementCounter ktora utworzy 
* nowy watek w tle z parametrem statycznej metody inkrementujacej pole typu 
* unsigned utworzonej (w funkcji) instancji klasy Counter.
*/

int main() {
    int numOfThreads = 4;
    std::vector<std::thread> threads;

    for (int i = 0; i < numOfThreads; i++){
        threads.push_back(std::thread(incrementCounter));
    }

    std::this_thread::sleep_for(std::chrono::milliseconds((1+numOfThreads) * 1000));

    for (int i = 0; i < numOfThreads; i++){
        threads[i].join();
    }
}

/* OUTPUT
8 -> modified by thread: [id watku 1]
9 -> modified by thread: [id watku 2]
10 -> modified by thread: [id watku 3]
11 -> modified by thread: [id watku 4]

// UWAGA!
Powyzsze id powinny byc niepowtarzalne, 
a odkomentowanie linii 26 
powinno spowodowac skrocenie linii outputu.
*/