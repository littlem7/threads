/*
* Plik źródłowy klasy Counter
* autor: Anna Bogusz
*/

#include "Counter.h"
#include <thread>
#include <iostream>

Counter *Counter::generatorInstance = nullptr;
std::mutex Counter::mtx;

Counter* Counter::getInstance(){
    if(!generatorInstance) {
        std::lock_guard<std::mutex> lock(mtx);
        if (!generatorInstance) {
            generatorInstance = new Counter();
        }
    }
    return generatorInstance;
}

Counter::~Counter(){
    delete generatorInstance;   
}

void Counter::increment(unsigned& value){
    std::lock_guard<std::mutex> lock(mtx);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    ++value;
    std::cout << value << " -> modified by thread: " << std::this_thread::get_id() << std::endl;
}