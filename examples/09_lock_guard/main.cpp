#include <iostream>
#include <mutex>
#include <thread>

std::mutex m;
unsigned counter=0;

unsigned increment()
{
    std::lock_guard<std::mutex> lk(m);
    std::cout << std::this_thread::get_id() << ": " << counter << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    return ++counter;
}


int main() {
    std::cout << "main: " << counter << std::endl;

    std::thread thd1(increment);
    std::thread thd2(increment);

    thd1.join();
    thd2.join();

    std::cout << "main: " << counter << std::endl;
}
// output:
/*
main: 0
140355496126208: 0
140355504518912: 1
main: 2
 */