#include <iostream>
#include <chrono>
#include <thread>

void independentThread(){
    std::cout << "Starting concurrent thread.\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));   // 6 sekund dla detach() spowoduje niewykonanie ostatniej instrukcji
    std::cout << "Exiting concurrent thread.\n";
}

void threadCaller(){
    std::cout << "Starting thread caller.\n";
    std::thread t(independentThread);
    // t.~thread();                                         // przykladowe wywolanie std::terminate()
    t.detach();
    // t.join();                                            // dla 6 sekund w linii 7 wykona ostatnia instrukcje
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "Exiting thread caller.\n";
}

int main(){
    threadCaller();
    std::this_thread::sleep_for(std::chrono::seconds(3));
}
// output
/*
Starting thread caller.
Starting concurrent thread.
Exiting concurrent thread.
Exiting thread caller.
*/