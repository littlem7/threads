#include <iostream>
#include <thread>

void func(){
    std::cout << "My first thread..." << std::this_thread::get_id() << std::endl;
}

struct A{
    static void static_memfun(){
        std::cout << "My second thread..." << std::this_thread::get_id() << std::endl;
    }
};

struct Functor{
    void operator()(){
        std::cout << "My third thread..." << std::this_thread::get_id() << std::endl;
    }
};

int main(){
    // watek pusty - not-a-thread
    std::thread zero;

    // wskaznik do funkcji
    std::thread first(func);

    // wskaznik do metody statycznej
    std::thread second(A::static_memfun);

    // obiekt funkcyjny
    Functor f;
    std::thread third(f);

    // PREZENTACJA JOINABLE - zwraca true, jesli obiekt watku identyfikuje aktywny watek wykonania

    std::thread fourth;
    std::cout << "before starting, joinable: " << fourth.joinable() << std::endl;

    // funkcja lambda
    fourth = std::thread([] {
        std::cout << "My fourth thread..." << std::this_thread::get_id() << std::endl;
    });
    std::cout << "after starting, joinable: " << fourth.joinable() << std::endl;

    // wstrzymanie wykonania funkcji main, aż do czasu zakończenia danego watku
    first.join();
    second.join();
    third.join();
    fourth.join();
    std::cout << "after joining, joinable: " << fourth.joinable() << std::endl;
}

// mozliwy (ze wzgledu na przeplatanie) output
/*
before starting, joinable: 0
after starting, joinable: 1
My first thread...140569843529472
My second thread...140569835136768
My third thread...140569826744064
My fifth thread...140569809958656
after joining, joinable: 0
*/