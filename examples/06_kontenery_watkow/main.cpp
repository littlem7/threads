#include <iostream>
#include <thread>
#include <vector>


void task(){
    std::cout << "task in thread: " << std::this_thread::get_id() << std::endl;
}  // funkcja wątku


int main() {
    std::vector<std::thread> threads;

    std::thread thd1(task);
    threads.push_back(std::move(thd1));
    threads.push_back(std::thread(task));
    threads.emplace_back(task);
    for(auto& t : threads)
        t.join();
}