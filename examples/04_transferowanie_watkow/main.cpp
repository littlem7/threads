#include <iostream>
#include <thread>

void task(){
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    std::cout << "task in thread: " << std::this_thread::get_id() << std::endl;
}

std::thread create_thread(){
    std::thread thd(&task);
    std::cout << "thread thd: " << thd.get_id() << std::endl;
    return std::move(thd);
}


int main() {

    std::thread t(&task);
    std::cout << "thread t: " << t.get_id() << std::endl;

    std::thread thread1 = std::move(t);
    std::cout << "thread thread1: " << thread1.get_id() << std::endl;

    std::thread thread2 = create_thread();

    thread1.join();
    thread2.join();

}

// mozliwy output
/*
thread t: 139736112924416
thread thread1: 139736112924416
thread thd: 139736104531712
task in thread: 139736112924416
task in thread: 139736104531712
*/


