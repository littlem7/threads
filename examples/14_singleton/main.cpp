#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

std::mutex _mtx;

class simpleSingleton {
    public:
        static simpleSingleton* pInstance;

        static simpleSingleton* getInstance(){
            if(!pInstance) {
                std::lock_guard<std::mutex> lock(_mtx);
                if (!pInstance) {
                    // std::lock_guard<std::mutex> lock(_mtx);      // lock tutaj moze spowodowac bledy
                    pInstance = new simpleSingleton();
                }
            }
            std::cout << pInstance << std::endl;
            return pInstance;
        }

    private:
        simpleSingleton(){}
};

simpleSingleton* simpleSingleton::pInstance = nullptr;

void genSingleton(){
    simpleSingleton::getInstance();
}

int main() {
    std::vector<std::thread> vec;
    for (int i = 0; i < 5; i++){
        vec.push_back(std::thread(genSingleton));
    }
    for (int i = 0; i < 5; i++){
        vec[i].join();
    }

}