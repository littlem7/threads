#include <iostream>
#include <functional>
#include <thread>


void handle_exception(std::exception_ptr eptr){     // passing by value is ok
    try{
        if (eptr){
            std::rethrow_exception(eptr);
        }
    }
    catch(const std::exception& e){
        std::cout << "Caught exception \"" << e.what() << "\"\n";
    }
}

void background_work(std::exception_ptr& eptr){
    try{
        std::string().at(1);                        // generuje std::out_of_range
    }
    catch(...){
        eptr = std::current_exception();            // przechwycenie wyjatku
    }
}

int main()
{
    std::exception_ptr eptr;                        // pusty wskaznik

    std::thread thd(background_work, std::ref(eptr));
    thd.join();

    handle_exception(eptr);
}                                                   // destruktor std::out_of_range jest wołany, gdy destruktor eptr